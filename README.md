# 1.簡介

​         綠界金流服務對於有收款需求的特店，提供完整的交易介接技術，並提供多種收款方式可供選擇，對消費者而言以達多元付款的便利性，包含信用卡、網路ATM、ATM櫃員機、超商代碼、超商條碼等，本文主要說明各種收款方式介接規格說明，與目前綠界金流服務提供的各種交易API的使用時機及方法。

  

# 2.交易流程說明  

## 信用卡訂單流程

```mermaid
sequenceDiagram
Note Left of USER:產生訂單
USER->>特店:結帳
特店->>特店: 成立訂單
特店->>綠界: 呼叫付款
綠界->>USER:導轉付款頁面
USER->>USER:選擇付款方式
USER->>綠界:確認付款
綠界->>收單銀行:信用卡授權
收單銀行->>綠界:授權結果
綠界->>特店:付款結果通知
綠界->>USER:付款結果畫面

```

## ATM/CVS/BARCODE訂單流程

```mermaid
sequenceDiagram
Note Left of USER:產生訂單
USER->>特店:結帳
特店->>特店: 成立訂單
特店->>綠界: 呼叫付款
綠界->>USER:導轉付款頁面
USER->>USER:選擇付款方式ATM/CVS/BARCODE
USER->>綠界:確認付款
綠界->>銀行(ATM)/超商:申請虛擬帳號/付款號碼
銀行(ATM)/超商->>綠界:取得虛擬帳號/付款號碼
綠界->>特店:取號結果通知
綠界->>USER:取號結果畫面
USER->>銀行(ATM)/超商:付款
銀行(ATM)/超商->>綠界:付款結果
綠界->>特店:付款結果通知
```


## 信用卡定期定額訂單流程



# 3.前置準備事項  

## 測試環境 

廠商管理後台     https://vendor-stage.ecpay.com.tw  

此網站可提供：1. 查詢ecpay訂單，2. 模擬付款並通知特店

| **欄位說明**                 | 一般特店               | 一般特店              | 平台商              |
| ---------------------------- | ---------------------- | --------------------- | ------------------- |
| 特店編號(MerchantID)         | 2000132                | 2000214(非OTP)        | 3002599             |
| 登入帳號/密碼                | stagetest1234/test1234 | Stage2000214/test1234 | stagetest2/test1234 |
| 身分證件末四碼<br />統一編號 | 53538851               | 53538851              | 3609                |
| HashKey                      | 5294y06JbISpM5x9       | 5294y06JbISpM5x9      | spPjZn66i0OhqJsQ    |
| HashIV                       |                        | v77hoKGq4kWxNNIS      | hT5OJckN45isQTTs    |

| **信用卡測試卡號**  | **4311-9522-2222-2222    <br />(****注意事項****：只有此組卡號可測試交易成功流程****)** |
| ------------------- | ------------------------------------------------------------ |
| 信用卡測試安全碼    | 222                                                          |
| 信用卡測試有效月/年 | 輸入的MM/YYYY值請大於現在當下時間的月年                      |

# 產生訂單

#### 應用場景

> 消費者在特店進行購物後送出訂單。<br/>
>  Step 1. 特店：將訂單資料以POST(HTTP Method)傳送至綠界，準備進行付款。<br/> Step 2. 綠界：接受特店訂單並檢核資料。

<font color="#DC143C" size=3> ※注意事項：</font>

> (1) 使用內置框架iframe可能會導致交易失敗，建議不要使用。<br/>
> (2) 若消費者使用環境為iOS系統時，請勿另開新視窗。詳細說明請參考。

#### 介接路徑

> 正式環境：https://payment.ecpay.com.tw/Cashier/AioCheckOut/V5 <br/>
> 測試環境：https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5



#### 特店傳入參數說明   ：

| 參數(<font color="#DC143C">\*</font>為必填)                | 參數名稱                       | 型態        | 說明 |
|---------------------|--------------------------------|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| <font color="#DC143C">\*</font>MerchantID        | 特店編號           | String(10)  | 1.[測試環境特店編號](#特店編號)<br />2.[正式環境金鑰取得](https://www.ecpay.com.tw/CascadeFAQ/CascadeFAQ_Qa?nID=1179)                  |
| <font color="#DC143C">\*</font>MerchantTradeNo   | 特店交易編號(由特店提供)       | String(20)  | 1.特店交易編號均為唯一值，不可重複使用。<br />2.英數字大小寫混合<br />3.[如何避免訂單編號重複請參考FAQ](https://www.ecpay.com.tw/CascadeFAQ/CascadeFAQ_Qa?nID=1454)<br/>4.如有使用PlatformID ，平台商底下所有商家之訂單編號亦不可重複。 |
| StoreID             | 特店旗下店舖代號               | String(20)  | 提供特店填入分店代號使用，僅可用英數字大小寫混合。                                                                                            |
| <font color="#DC143C">\*</font>MerchantTradeDate | 特店交易時間                   | String(20)  | 格式為：`yyyy/MM/dd HH:mm:ss`                                                                                                                 |
| <font color="#DC143C">\*</font>PaymentType       | 交易類型                       | String(20)  | 請固定填入 aio                                     |
| <font color="#DC143C">\*</font>TotalAmount       | 交易金額                       | Int         | 請帶整數，不可有小數點。<br />僅限新台幣。<br />請參考[各付款方式金額的限制](<https://www.ecpay.com.tw/CascadeFAQ/CascadeFAQ_Qa?nID=3605>) |
| <font color="#DC143C">\*</font>TradeDesc         | 交易描述                       | String(200) | 傳送到綠界前，請將參數值先做UrlEncode。                                                                                                       |
| <font color="#DC143C">\*</font>ItemName          | 商品名稱                       | String(200) | 1. 如果商品名稱有多筆，需在金流選擇頁一行一行顯示商品名稱的話，商品名稱請以符號\#分隔。<br />2. 商品名稱字數限制為中文60字或英數120字內，超過此限制系統將自動截斷。          |
| <font color="#DC143C">\*</font>ReturnURL         | 付款完成通知回傳網址           | String(200) | 當消費者付款完成後，綠界會將付款結果參數以幕後(Server POST)回傳到該網址。<br />詳細說明請參考[付款結果通知](#付款結果通知v2)<br /><font color="#DC143C" size=3> ※注意事項：</font><br />1.請勿設定與Client端接收付款結果網址OrderResultURL相同位置，避免程式判斷錯誤。<br />2.請在收到Server端付款結果通知後，請正確回應1\|OK給綠界。 |
| <font color="#DC143C">\*</font><span id="ChoosePayment">ChoosePayment</span>     | 選擇預設付款方式               | String(20)  | 綠界提供下列付款方式，請於建立訂單時傳送過來:<br/>`Credit`：信用卡及銀聯卡<br/>`UnionPay`：銀聯卡(需申請開通)<br/>`WebATM`：網路ATM<br/>`ATM`：自動櫃員機<br/>`CVS`：超商代碼<br/>`BARCODE`：超商條碼<br/>`ALL`：不指定付款方式，由綠界顯示付款方式選擇頁面。<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>1.若為手機版時不支援下列付款方式:<br/>`WebATM`：網路ATM<br/>`BARCODE`：超商條碼 |
| <font color="#DC143C">\*</font>CheckMacValue     | 檢查碼                         | String      | 請參考附錄[檢查碼機制](#檢查碼機制)與產生檢查碼範例程式 |
| <span id="ClientBackURL">ClientBackURL</span>       | Client端返回特店的按鈕連結     | String(200) | 消費者點選此按鈕後，會將頁面導回到此設定的網址<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>導回時不會帶付款結果到此網址，只是將頁面導回而已。<br/>設定此參數，綠界會在付款完成或取號完成頁面上顯示[返回商店]的按鈕。 <br/>設定此參數，發生簡訊OTP驗證失敗時，頁面上會顯示[返回商店]的按鈕。<br/>若未設定此參數，則綠界付款完成頁或取號完成頁面，不會顯示[返回商店]的按鈕。<br/>若導回網址未使用https時，部份瀏覽器可能會出現警告訊息。 |
| ItemURL             | 商品銷售網址                   | String(200) |                                                                                                                                               |
| Remark              | 備註欄位。                     | String(100) |                                                                                                                                               |
| ChooseSubPayment    | 付款子項目                     | String (20) | 若設定此參數，建立訂單將轉導至綠界訂單成立頁，依設定的付款方式及付款子項目帶入訂單，無法選擇其他付款子項目。請參考[付款方式一覽表](#付款方式) |
| OrderResultURL      | Client端回傳付款結果網址       | String(200) | 當消費者付款完成後，綠界會將付款結果參數以幕前(Client POST)回傳到該網址。<br/>詳細說明請參考付款結果通知<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>1. 若與[ClientBackURL](#ClientBackURL)同時設定，將會以此參數為主。<br/>2銀聯卡及非即時交易(ATM、CVS、BARCODE)不支援此參數。 |
| NeedExtraPaidInfo   | 是否需要額外的付款資訊         | String(1)   | 額外的付款資訊：<br/>若不回傳額外的付款資訊時，參數值請傳：N；<br/>若要回傳額外的付款資訊時，參數值請傳：Y，付款完成後綠界會以Server POST方式回傳額外付款資訊。<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>回傳額外付款資訊參數請參考-額外回傳的參數 |
| DeviceSource        | 裝置來源                       | String(10)  | 請帶空值，由系統自動判定。                                                                                                                    |
| IgnorePayment       | 隱藏付款方式                   | String(100) | 當付款方式[ChoosePayment](#ChoosePayment)為ALL時，可隱藏不需要的付款方式，多筆請以井號分隔(\#)。<br />可用的參數值：<br/>`Credit`:信用卡<br/>`WebATM`:網路ATM<br/>`ATM:自動櫃員機`<br/>`CVS`:超商代碼<br/>`BARCODE`:超商條碼 |
| PlatformID          | 特約合作平台商代號(由綠界提供) | String(10)  | 為專案合作的平台商使用。<br/>一般特店或平台商本身介接，則參數請帶放空值。<br/>若為專案合作平台商的特店使用時，則參數請帶平台商所綁的特店編號[MerchantID]。                              |
| InvoiceMark         | 電子發票開立註記               | String(1)   | 此參數為付款完成後同時開立電子發票。<br/>若要使用時，該參數須設定為「Y」，<br/>同時還要設定「電子發票介接相關參數」<br/><br/><font color="#DC143C" size=3> ※注意事項：</font><br/>正式環境欲使用電子發票功能，須與綠界申請開通，若未開通請致電客服中心 (02) 2655-1775。 |
| CustomField1        | 自訂名稱欄位1                  | String(50)  | 提供合作廠商使用記錄用客製化使用欄位<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>特殊符號只支援,.#()$[];%{}:/?&@<>!                                              |
| CustomField2        | 自訂名稱欄位2                  | String(50)  | 提供合作廠商使用記錄用客製化使用欄位<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>特殊符號只支援,.#()$[];%{}:/?&@<>!                                              |
| CustomField3        | 自訂名稱欄位3                  | String(50)  | 提供合作廠商使用記錄用客製化使用欄位<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>特殊符號只支援,.#()$[];%{}:/?&@<>!                                              |
| CustomField4        | 自訂名稱欄位4                  | String(50)  | 提供合作廠商使用記錄用客製化使用欄位<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>特殊符號只支援,.#()$[];%{}:/?&@<>!                                              |
| <font color="#DC143C">\*</font>EncryptType       | CheckMacValue加密類型          | Int         | 請固定填入1，使用SHA256加密。                                                                                                                 |

> 範例
> ```json
> "MerchantID":"2000132",
> "MerchantTradeNo":"ecPay1234",
> "StoreID":"TaipeiZoo101",
> "MerchantTradeDate":"2012/03/21 15:40:18",
> "PaymentType":"aio",
> "TotalAmount":5000,
> "TradeDesc":"ecpay商城購物",
> "ItemName":"手機20元X2#隨身碟60元X1",
> "ReturnURL":"http://your.web.site/receive.php",
> "ChoosePayment":"Credit",
> "CheckMacValue":"",
> "ClientBackURL":"http://your.web.site/Shopping/Detail",
> "ItemURL":"",
> "Remark":"",
> "ChooseSubPayment":"TAISHIN",
> "OrderResultURL":"http://your.web.site/client.php",
> "NeedExtraPaidInfo":"N",
> "DeviceSource":"",
> "IgnorePayment":"ATM#WebATM",
> "PlatformID":"",
> "InvoiceMark":"",
> "CustomField1":"",
> "CustomField2":"",
> "CustomField3":"",
> "CustomField4":"",
> "EncryptType":1
> ```

- 當ChoosePayment參數為使用ALL或ATM付款方式時：

  | 參數 | 參數名稱 | 型態 | 說明 |
  | -------- | ------------ | -------- | -------- |
  | ExpireDate | 允許繳費有效天數 | Int | 若需設定最長 60 天，最短1天。<br/>未設定此參數則預設為3天<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>以天為單位 |
  | PaymentInfoURL | Server端回傳付款相關資訊 | String(200) | 若有設定此參數，訂單建立完成後(非付款完成)，綠界會Server端背景回傳消費者付款方式相關資訊<br />(例：銀行代碼、繳費虛擬帳號繳費期限…等)。<br/>請參考[ATM、CVS或BARCODE的取號結果通知.]<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>頁面將會停留在綠界，顯示繳費的相關資訊。 |
  | ClientRedirectURL | Client端回傳付款相關資訊 | String (200) | 若有設定此參數，訂單建立完成後(非付款完成)，綠界會Client端回傳消費者付款方式相關資訊<br />(例：銀行代碼、繳費虛擬帳號繳費期限…等)且將頁面轉到特店指定的頁面。<br />請參考[ATM、CVS或BARCODE的取號結果通知.]<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>若設定此參數，將會使設定的返回特店的按鈕連結[ClientBackURL]失效。<br/>若導回網址未使用https時，部份瀏覽器可能會出現警告訊息。 |
  
  > <font color="#DC143C" size=3> ※注意事項：</font>
  > 各銀行ATM繳款帳號，若金額錯誤、逾期繳費、重覆繳款，是經由銀行端機制進行檢核ATM繳款帳號資訊，綠界科技無法進行金額錯誤、逾期繳費、重覆繳款的訂單阻擋。
  
  > 範例
  >
  > ```json
  > "ExpireDate":7,
  > "PaymentInfoURL":"http://your.web.site/paymentinfo.php",
  > "ClientRedirectURL":"http://your.web.site/ClientRedirectURL.php"
  > ```


- 當ChoosePayment參數為使用ALL或CVS或BARCODE付款方式時：

  | 參數            | 參數名稱         | 型態       | 說明                                                         |
  | --------------- | ---------------- | ---------- | ------------------------------------------------------------ |
  | StoreExpireDate | 超商繳費截止時間 | int        | <font color="#DC143C" size=3> ※注意事項：</font><br/>CVS:以分鐘為單位<br/>BARCODE:以天為單位<br/>若未設定此參數，皆為預設值7天<br/>若需設定此參數，請於建立訂單時將此參數送給綠界<br/>例：08/01的20:15分購買商品，繳費期限為7天，表示8/08的20:15分前您必須前往超商繳費。 |
  | Desc_1    | 交易描述1    | String(20) | 會出現在超商繳費平台螢幕上    |
  | Desc_2    | 交易描述2    | String(20) | 會出現在超商繳費平台螢幕上    |
  | Desc_3    | 交易描述3    | String(20) | 會出現在超商繳費平台螢幕上    |
  | Desc_4    | 交易描述4    | String(20) | 會出現在超商繳費平台螢幕上    |
  | PaymentInfoURL | Server端回傳付款相關資訊 | String(200) | 若有設定此參數，訂單建立完成後(非付款完成)，綠界會Server端背景回傳消費者付款方式相關資訊<br />(例：繳費代碼與繳費超商)。<br/>請參考[ATM、CVS或BARCODE的取號結果通知.]<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>頁面將會停留在綠界，顯示繳費的相關資訊。<br/>回傳只有三段號碼，並不會回傳條碼圖，需自行轉換成code39的三段條碼。 |
  | ClientRedirectURL | Client端回傳付款方式相關資訊 | String(200) | 若有設定此參數，訂單建立完成後(非付款完成)，綠界會從Client端回傳消費者付款方式相關資訊<br />(例：繳費代碼與繳費超商)且將頁面轉到特店指定的頁面。<br/>請參考[ATM、CVS或BARCODE的取號結果通知.]<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>若設定此參數，將會使設定的返回特店的按鈕連結[ClientBackURL]失效。<br/>若導回網址未使用https時，部份瀏覽器可能會出現警告訊息。<br/>回傳只有三段號碼，並不會回傳條碼圖，需自行轉換成code39的三段條碼。 |

  > 範例
  >
  > ```json
  > "StoreExpireDate":14400,
  > "Desc_1":"交易描述1",
  > "Desc_2":"交易描述2",
  > "Desc_3":"交易描述3",
  > "Desc_4":"交易描述4",
  > "PaymentInfoURL":"",
  > "ClientRedirectURL":""
  > ```

- 當ChoosePayment參數為ALL或Credit付款方式時：

  | 參數            | 參數名稱         | 型態       | 說明            |
  | ---- | ---- | ---- | ---- |
  | BindingCard | 記憶卡號 | int | 使用記憶信用卡<br/>使用：請傳1<br/>不使用：請傳0 |
  | MerchantMemberID | 記憶卡號識別碼 | String(30) | 1.【記憶卡號於平台商PlatformID】 <br/>1-1命名規則由平台商制定(適用有會員機制的平台商)：傳送MerchantMemberID參數時，前七碼必為平台商PlatformID<br /><font color="#DC143C" size=3>(平台商代號PlatformID+會員識別碼，長度不得超過30個字元)</font><br/>Ex: 3002599Test1234<br/>1-2命名規則由平台底下的特店制定(適用於有會員機制的特店)：傳送MerchantMemberID參數時，前七碼必為特店之MerchantID<br /> <font color="#DC143C" size=3>(特店代號MerchantID+會員識別碼，長度不得超過30個字元)</font><br/>Ex: 2000132Test1234<br/>2.【記憶卡號於特店MerchantID】命名規則由特店制定(適用於有會員機制的特店)：傳送MerchantMemberID參數時，前七碼必為特店之MerchantID<br /><font color="#DC143C" size=3>(特店代號MerchantID+會員識別碼，長度不得超過30個字元)</font><br/>Ex: 2000132Test1234 |

  > <font color="#DC143C" size=3> ※注意事項：</font>
  > (1) 「欲使用BindingCard、MerchantMemberID這兩個參數功能，特店必須有會員系統。」
  > (2) 若記憶卡號識別碼為平台商的會員識別碼時，要特別向綠界申請使用。
  > (3) 記憶卡號功能僅支援Visa/ MasterCard/ JCB，不支援銀聯卡。
  
  > 範例
  >
  > ```json
  > "BindingCard":1,
  > "MerchantMemberID":"Test1234"
  > ```



- 當ChoosePayment參數為Credit付款方式時：

  | 參數            | 參數名稱         | 型態       | 說明            |
  | ---- | ---- | ---- | ---- |
  | Language | 語系設定 | String(3) | 預設語系為中文，若要變更語系參數值請帶：<br/>英語：ENG<br/>韓語：KOR<br/>日語：JPN<br/>簡體中文：CHI<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>1. 使用語系設定時，系統將不支援信用卡記憶卡號功能<br/>2. 不支援手機版 |

  > 範例
  >
  > ```json
  > "Language":"ENG"
  > ```



- 當ChoosePayment參數為ALL或Credit付款方式時：

  - 一次付清：此收款方式消費者可選擇是否使用信用卡紅利折抵。
    下列為一次付清參數，若您需使用此功能，以下參數必須傳送給綠界：

    | 參數            | 參數名稱         | 型態       | 說明            |
    | ---- | ---- | ---- | ---- |
    | Redeem | 信用卡是否使用紅利折抵。 | String(1) | 設為Y時，當綠界特店選擇信用卡付款時，會進入紅利折抵的交易流程。<br/><font color="#DC143C" size=3> ※注意事項：</font><br/>紅利折抵請參考[信用卡紅利折抵辦法](<https://pay.ecpay.com.tw/CreditPayment/CreditRedeemInfo>) |
    | UnionPay | 是否為銀聯卡交易 | Int<br />預設值：0 | 是否為銀聯卡。<br/>若為否時，請帶：0<br/>若為是時，請帶：1<br/>當此參數帶1，綠界會將頁面導到銀聯網站。<br/><font color="#DC143C" size=3>※注意事項：</font><br/>1.若需使用銀聯卡服務，請與綠界提出申請方可使用，測試環境未提供銀聯卡服務。<br/>2.不支援信用卡分期付款及定期定額。<br/>3.不支援信用卡紅利折抵 |
    
    > 範例
    >
    > ```json
    > "Redeem":"Y",
    > "UnionPay":0
    > ```
    
    
    
  - 分期付款：此收款方式消費者只需刷一次卡做信用卡授權，後續分期金額由銀行端執行確認。
    下列為分期付款參數，若您需使用此功能，以下參數必須傳送給綠界：
  
    | 參數            | 參數名稱         | 型態       | 說明            |
    | ---- | ---- | ---- | ---- |
    | <font color="#DC143C" size=3>*</font>CreditInstallment | 刷卡分期期數 | String(20) | 提供刷卡分期期數<br/>信用卡分期可用參數為:3,6,12,18,24<br/><font color="#DC143C" size=3>注意事項：</font><br/>使用的期數必須先透過申請開通後方能使用，並以申請開通的期數為主。 |
  
    > <font color="#DC143C" size=3>注意事項：</font>
    > (1) 不可以與信用卡定期定額、紅利折抵參數一起設定。
    > (2) 若使用分期付款功能，後續分期的款項會由銀行執行確認，相關銀行可使用分期期數請參考銀行分期期數。
    > (3) 欲在測試環境進行刷卡功能，請使用綠界提供的信用卡測試卡號進行模擬付款。
    > (4) 串接時請帶訂單的刷卡分期的總付款金額，無須自行計算各分期金額，除不盡的金額銀行會於第一期收取。舉例：總金額 1733元 分 6 期，除不盡的放第一期，293，288，288，288，288，288
    > (5) 銀聯卡不支援分期付款方式
    
    > 範例
    > ```json
    > "CreditInstallment":"3,6
    > ```